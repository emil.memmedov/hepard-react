import classes from './sections.module.css';

const Section = (props) =>{
    return(
        <div className = {classes.Section}>
            <span className = {classes.SectionImg}>{props.image}</span>
            <h1 className = {classes.Heading}>{props.heading} orders</h1>
            <p className = {classes.Paragraph}>{props.paragraph}</p>
        </div>
    )
}
export default Section;