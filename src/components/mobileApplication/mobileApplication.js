import classes from './mobileApplication.module.css';

const MobileApplication = () =>{
    return (
        <div className = {classes.MobileApplication}>
            <div className = {classes.Images}>
                <img className = {classes.ImageBack} src = "./images/phoneBack.png" />
                <img className = {classes.ImageFront} src = "./images/phoneFront.png" />
            </div>
            <div className = {classes.MobImages}>
                <img className = {classes.MobImageBack} src = "./images/appMobBack.png" />
                <img className = {classes.MobImageFront} src = "./images/appMobFront.png" />
            </div>
            <div className = {classes.Writing}>
                <h1 className = {classes.Heading}>It is a long established fact that a reader will be distracted by the </h1>
                <p className = {classes.Paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make </p>
                <ul className = {classes.List}>
                    <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Integrate your delivery</li>
                    <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Evaluate your customers</li>
                    <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Track your delivery</li>
                    <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Evaluate your customers</li>   
                </ul>
                <button className = {classes.BtnMoreAbout}>Mobile application</button>
            </div>
            <div className = {classes.Line}>
                <svg width="1150" height="650" viewBox="0 0 1150 650" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M555.66 370.467C1611.58 637.76 420.093 -395.009 1553.59 174.087C2687.09 743.183 1512.7 664.717 1512.7 664.717C1341.48 604.888 1040.39 477.749 1074.3 811.915C1233.44 1040.96 1234.23 1193.64 1009.47 1257.43C784.708 1321.23 719.43 1125.87 356.746 1260.43C-5.93845 1394.98 3.98191 1008.27 119.934 867.965C311.474 636.201 -500.26 103.174 555.66 370.467Z" fill="white" fill-opacity="0.71"/>
                </svg>
            </div>
            <div className = {classes.MobilLine}>
                <svg width="375" height="590" viewBox="0 0 375 590" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M53.4527 326.607C564.209 624.625 42.7435 -401.076 569.852 184.895C1096.96 770.866 512.48 645.737 512.48 645.737C430.895 581.868 289.077 448.874 281.049 765.441C343.86 988.341 332.799 1132.34 215.025 1182.7C97.251 1233.07 79.0957 1046.02 -113.324 1157.1C-305.743 1268.18 -271.737 903.974 -202.92 776.731C-89.2408 566.539 -457.303 28.5893 53.4527 326.607Z" fill="white" fill-opacity="0.71"/>
                </svg>
            </div>

        </div>
    )
}

export default MobileApplication;