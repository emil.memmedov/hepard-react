import classes from './aboutPartnership.module.css';

const AboutPartnership = () =>{
    return (
        <div className = {classes.AboutPartnership}>
            <div className = {classes.Line}>
                <svg width="680" height="696" viewBox="0 0 680 696" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M-0.160908 13.1466C-0.160908 13.1466 100.57 -20.1152 299.926 22.7733C425.319 49.7496 504.782 116.193 487.268 190.675C471.052 259.631 509.36 306.256 625.865 336.712C722.272 402.49 717.354 602.574 372.413 587.614C198.5 546.994 171.323 746.942 -3.58112 682.722L-0.160908 13.1466Z" fill="#D1DDF5" fill-opacity="0.07"/>
                </svg>
            </div>
            <div className = {classes.MobilLine}>
                <svg width="277" height="610" viewBox="0 0 277 610" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M-52.7876 16.4371C-52.7876 16.4371 -5.71849 -16.8531 89.3613 12.1984C149.165 30.4713 187.938 85.1437 180.97 150.923C174.518 211.822 193.468 250.922 249.129 272.608C295.906 325.985 297.122 500.914 133.657 502.386C50.655 474.243 41.337 649.991 -42.5516 601.281L-52.7876 16.4371Z" fill="#D1DDF5" fill-opacity="0.07"/>
                </svg>
            </div>
            <div className = {classes.Images}>
                <img className = {classes.ImageFront} src = "./images/PartnerFront.png" />
                <img className = {classes.ImageBack} src = "./images/PartnerBack.png" />
            </div>
            <div className = {classes.MobImages}>
                <img className = {classes.MobImageFront} src = "./images/mobPartnerFront.png" />
                <img className = {classes.MobImageBack} src = "./images/mobPartnerBack.png" />
            </div>
            <h1 className = {classes.Heading}>It is a long established fact that a reader will be distracted by the </h1>
            <p className = {classes.Paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make </p>
            <ul className = {classes.List}>
                <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Integrate your delivery</li>
                <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Evaluate your customers</li>
                <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Track your delivery</li>
                <li className = {classes.ListItem}><span className = {classes.ListIcon}><img src = "./images/listitem.png" /></span>Evaluate your customers</li>   
            </ul>
            <button className = {classes.BtnMoreAbout}>More About Partnership</button>
        </div>
    )
}
export default AboutPartnership;