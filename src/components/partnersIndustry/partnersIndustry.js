import React, {Component} from 'react';
import classes from './partnersIndustry.module.css';
import './style.css';

var xDown = null;                                                        
var yDown = null;
class PartnershipIndustry extends Component{
    state = {
        images: [
            {imagePath: "./images/all.svg",name: 'All'},
            {imagePath:"./images/food.svg",name: 'Food'},
            {imagePath: "./images/medicine.svg",name: 'Medicine'},
            {imagePath: "./images/parfum.svg",name: 'Parfum'},
            {imagePath: "./images/clothes.svg",name: 'Clothes'},
            {imagePath: "./images/gift.svg", name: 'Gift'}
        ],
        sections: {
            food: [
                {path: './images/kfcIcon.png', heading: 'KFC', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kosebasiIcon.png', heading: 'Kosebasi', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/burgerkingIcon.png', heading: 'Burger King', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/doneristIcon.png', heading: 'Donerist', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kosebasiIcon.png', heading: 'Kosebasi', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kfcIcon.png', heading: 'KFC', p: 'Lorem Ipsum is simply dummy text of the printing and '},
                {path: './images/kosebasiIcon.png', heading: 'Kosebasi', p: 'Lorem Ipsum is simply dummy text of the printing and '}
            ],
            medicine: [],
            parfum: [],
            clothes: [],
            gift: []
        },
        active: {
            all:true,
            food: false,
            medicine: false,
            parfum: false,
            clothes: false,
            gift: false
        },
        karuselImages: [],
        designed: false,
        shown: 3, 
        karuselStart: 0,
        karuselEnd: 7,
        karuselNull: false
    }
    MainSectionClickHandler = (e) =>{
        // window.screen.width
        if(e.target.getAttribute('id') === "Parfum"){
            if(window.screen.width < 480){
                document.getElementsByClassName(classes.MainSection)[2].style= "margin-right:12px;";
                document.getElementsByClassName(classes.MainSection)[3].style= "margin-right:12px;";
                document.getElementsByClassName(classes.MainSection)[5].style= "margin-right:12px;";
                document.getElementsByClassName(classes.MainSection)[4].style= "left: 0px; margin-right:12px;";
                document.getElementsByClassName(classes.MainSections)[0].style = "left: -166px; width: 155%";
                document.getElementsByClassName(classes.MainSection)[1].style= "transition: all 1.2s; transform: translate(-30%);";
                document.getElementsByClassName(classes.ImageP)[1].style = "position: absolute; top:144%;";
            }
        }
        if(e.target.getAttribute('id') === "Medicine"){
            if(window.screen.width < 480){
                document.getElementsByClassName(classes.MainSection)[2].removeAttribute('style');
                document.getElementsByClassName(classes.MainSection)[3].removeAttribute('style');
                document.getElementsByClassName(classes.MainSection)[5].removeAttribute('style');
                document.getElementsByClassName(classes.MainSection)[4].removeAttribute('style');
                document.getElementsByClassName(classes.MainSections)[0].removeAttribute('style');
                document.getElementsByClassName(classes.MainSection)[1].removeAttribute('style');
                document.getElementsByClassName(classes.ImageP)[1].removeAttribute('style');
            }
        }
        const id = e.target.getAttribute('id');
        const active = this.state.active;
        for(const key in this.state.active){
            if(key === id.toLowerCase()){
                active[key] = true;
            }
            else{
                active[key] = false;
            }
        }
        this.setState({active: active});
        this.KaruselImages();
    }
    AllImages = () =>{
        const karuselImages = [];
        let i = 0;
        for(const section in this.state.sections){
            if(this.state.sections[section].length>0){
                for(let index in this.state.sections[section]){
                    karuselImages.push(this.state.sections[section][index]);
                }
            }
            else{
                i++;
            }
        }
        if(i>=5){
            this.setState({karuselNull: true});
        }
        else{
            this.setState({karuselNull: false});
        }
        var karuselIndex = 0;
        if(karuselImages.length < 7){
            for(let i = karuselImages.length; i < 7;i++){
                karuselImages.push(karuselImages[karuselIndex++]);
            }
        }
        this.setState({karuselImages: karuselImages});
        this.setState({karuselEnd: karuselImages.length-1});
        this.setState({designed: true});
    }
    
    handleTouchStart = (evt) =>{
        const firstTouch = evt.touches[0];                                      
            xDown = firstTouch.clientX;                                      
            yDown = firstTouch.clientY;
    }
    handleTouchMove = (evt) =>{
        if ( ! xDown || ! yDown ) {
            return;
        }

        var xUp = evt.touches[0].clientX;                                    
        var yUp = evt.touches[0].clientY;

        var xDiff = xDown - xUp;
        var yDiff = yDown - yUp;

        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
            if ( xDiff > 0 ) {
                this.RoundChange(4);
            } else {
                this.RoundChange(2);
            }                       
        }
        xDown = null;
        yDown = null;          
    }
    componentDidMount = () =>{
        this.AllImages();
        document.addEventListener('scroll',()=>{
            console.log(document.getElementsByClassName(classes.PartnershipIndustry)[0].getBoundingClientRect().top);
            if(document.getElementsByClassName(classes.PartnershipIndustry)[0].getBoundingClientRect().top<164 && document.getElementsByClassName(classes.PartnershipIndustry)[0].getBoundingClientRect().top > -595){
                document.onkeydown = (event)=>{
                    console.log(event);
                    if(event.key === 'ArrowRight'){
                        this.RoundChange(4);
                    }
                    else if(event.key === 'ArrowLeft'){
                        this.RoundChange(2);
                    }   
                };
                document.addEventListener('touchstart', this.handleTouchStart, false);        
                document.addEventListener('touchmove', this.handleTouchMove, false);
            }
            else{
                document.removeEventListener('touchstart', this.handleTouchStart);
                document.removeEventListener('touchmove', this.handleTouchMove);
                document.onkeydown = null;
            }
        });
    }
    ThisImage = (section) =>{
        let karuselImages = [];
        karuselImages = this.state.sections[section];
        if(karuselImages.length === 0){
            this.setState({karuselNull: true});
        }
        else{
            this.setState({karuselNull: false});
        }
        console.log(karuselImages);
        this.setState({karuselImages: karuselImages});
    }
    KaruselImages = () =>{
        for(const key in this.state.active){
            if(this.state.active[key]){
                if(key === 'all'){
                    this.AllImages();
                }
                else{
                    this.ThisImage(key);
                }
            };
        }
    }
    RightChange = (index) =>{
        var karuselStart = this.state.karuselStart;
        var karuselEnd = 6;
        for(var i = karuselStart; i <= karuselEnd; i++){
            if((i - (index-3)) < 0){
                document.getElementById('id' + i).id = 'id' + (karuselEnd + 1);
                karuselEnd++;
            }
            else{
                if(i - (index-3) > 6){
                    // document.getElementById('id' + i).style = "width: 0; position: absolute; right: -500px; transition: all 0s;";
                    document.getElementById('id' + i).id = 'id' + (i - (index-3));
                }
                else{
                    document.getElementById('id' + i).id = 'id' + (i - (index-3));
                    document.getElementById('id' + (i - (index-3))).removeAttribute('style');
                    if((i - (index-3)) === 0){
                        document.getElementById('id0').style = "transition: all 1.2s";
                    }
                }
            }                
        }
        this.setState({karuselEnd: this.state.karuselImages.length - 1});
        this.setState({karuselStart: 0});
    }
    LeftChange = (index) =>{
        var karuselStart = this.state.karuselStart;
        var karuselEnd = this.state.karuselEnd;
        for(var i = karuselEnd; i >= karuselStart; i--){
            if((i + (3 - index)) > 6){
                document.getElementById('id' + i).id = 'id' + (karuselStart - 1);
                karuselStart--;
            }
            else{
                document.getElementById('id' + i).id = 'id' + (i + (3 - index));
                if(i + (3 - index) >= 0){
                    if((i + (3-index)) === 0){
                        document.getElementById('id0').style = "transition: all 0s";
                    }
                    document.getElementById('id' + (i + (3 - index))).removeAttribute('style');
                    if((i + (3-index)) === 6){
                        document.getElementById('id6').style = "transition: all 1.2s";
                    }
                }

            }
        }
        this.setState({karuselStart: karuselStart + (3 - index)});
        this.setState({karuselEnd: 6});
    }
    ChangeImage = (index) =>{
        if(index < 3){
            this.LeftChange(index);
        }
        if(index > 3){
            this.RightChange(index);
        }
    }
    RoundChange = (index) =>{
        if(document.getElementById('id0')){
        if(index > 3 ){ //rightChange
            for(let i = -3; i <= 9; i++){
                document.getElementById('r' + i).id = 'r' + (i - (index - 3));
                if((i - (index - 3)) < 0){
                    document.getElementById('r' + (i - (index - 3))).style = "position: relative; width: 0;";
                }
                else if((i - (index - 3)) > 6){
                    document.getElementById('r' + (i - (index - 3))).style = "position: relative; right: -80px; width: 0;";
                }
                else
                document.getElementById('r' + (i - (index - 3))).removeAttribute('style');
            }
        }
        else if(index < 3){ //leftChange
            for(let i = 9; i >= -3; i--){
                document.getElementById('r' + i).id = 'r' + (i + (3 - index));
                if((i + (3-index)) < 0){
                    document.getElementById('r' + (i + (3-index))).style = "position: relative; width: 0;"
                }
                else if((i + (3 - index)) > 6){
                    document.getElementById('r' + (i + (3-index))).style = "position: relative; right: -80px; width: 0;"
                }
                else
                document.getElementById('r' + (i + (3 - index))).removeAttribute('style');
            }
        }
        if(index > 3)
        for(let i = (-3 - (index - 3)); i < -3; i++){
            if(i < -3){
                document.getElementById('r' + i).id = 'r' + (9 - ((i+3)*-1 - 1));
            }
        }
        else if (index < 3)
        for(let i = (9 + (3-index)); i > 9; i--){
            if(i > 9){
                document.getElementById('r' + i).id = 'r' + (-3 + ((i-9)-1)); ///something wrong
            }
        }

        this.ChangeImage(index);
    }
    }
    RoundClickHandler = (e) =>{
        let index = parseInt(e.target.getAttribute('id').slice(1));
        this.RoundChange(index);
    }
    render = () => {
        return (
            <div className = {classes.PartnershipIndustry}>
                <h1 className = {classes.Heading}>Partners Industry</h1>
                <div className = {classes.KaruselSection}>
                    <div className = {classes.MainSections}>
                        {this.state.images.map(image=>{
                            return(
                                <div id = {image.name} className = {classes.MainSection} onClick = {this.MainSectionClickHandler}>
                                    <img id = {image.name} src = {image.imagePath}/>
                                    <p id = {image.name} className = {this.state.active[image.name.toLowerCase()]? [classes.ImageP, classes.Active].join(' '): classes.ImageP}>{image.name}</p>
                                </div>
                            )
                        })}
                    </div>
                    <div className = {classes.Karusel}>
                        {this.state.karuselImages.map((image,i)=>{
                            return(
                                <div id = {"id" + i} style = {i>6?{position: 'absolute', right: '-500px'}:null} className = {[classes.section,classes.id].join(' ')} >
                                    <div className = {classes.imgBack}>
                                        <img className = {classes.KaruselImage} src= {image.path} />
                                    </div>
                                    <h1 className = {classes.KaruselHeading}>{image.heading}</h1>
                                    <p className = {classes.KaruselP}>{image.p}</p>
                                </div>
                            )
                        })}
                    </div>
                    <div className = {classes.ImageDetection}>
                        {!this.state.karuselNull?[-3,-2,-1,0,1,2,3,4,5,6,7,8,9].map((inn)=>{
                            return(
                                <div onClick = {this.RoundClickHandler} id = {'r' + inn} style = {{width: inn<0?'0' :inn>6?'0':null}}className = {classes.Round}>
                                    
                                </div>
                            )
                        }):null}
                    </div>
                </div>
            </div>
        )
    }
}
export default PartnershipIndustry;