import classes from './smartphone.module.css';

const Smartphone = () =>{
    const gpIcon = 
    <svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M3.19691 0.305861C2.57862 -0.0663303 1.84174 -0.0996118 1.19907 0.210235L11.9743 11.0085L15.4975 7.4689L3.19691 0.305861ZM0.209064 1.20962C0.0731257 1.49743 0 1.81759 0 2.14713V21.8995C0 22.2304 0.0735945 22.5459 0.21094 22.8276L10.982 12.0055L0.209064 1.20962ZM11.9758 13.0011L1.21787 23.8102C1.50334 23.9423 1.8071 24 2.11038 24C2.48491 24 2.85851 23.9105 3.19695 23.707L15.4999 16.5332L11.9758 13.0011ZM20.1823 10.1971C20.1781 10.1942 20.1734 10.1914 20.1691 10.1891L16.7538 8.20016L12.9677 12.0041L16.7561 15.8005C16.7561 15.8005 20.1781 13.8055 20.1823 13.8032C20.8174 13.4174 21.1967 12.7433 21.1967 11.9999C21.1967 11.2564 20.8174 10.5824 20.1823 10.1971Z" fill="#2A52A0"/>
    </svg>;
    const appleIcon = 
    <svg width="21" height="24" viewBox="0 0 21 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M14.749 0C13.4695 0.0884995 11.974 0.907495 11.1025 1.97399C10.3075 2.94148 9.65353 4.37847 9.90852 5.77497C11.3065 5.81847 12.751 4.97997 13.588 3.89548C14.371 2.88598 14.9635 1.45799 14.749 0Z" fill="white"/>
        <path d="M19.8053 8.05167C18.5768 6.51118 16.8503 5.61719 15.2198 5.61719C13.0674 5.61719 12.1569 6.64768 10.6614 6.64768C9.11938 6.64768 7.94789 5.62019 6.0864 5.62019C4.25791 5.62019 2.31092 6.73768 1.07643 8.64867C-0.659058 11.3397 -0.36206 16.3991 2.45042 20.7086C3.45692 22.2506 4.80091 23.9846 6.5589 23.9996C8.12339 24.0146 8.56439 22.9961 10.6839 22.9856C12.8034 22.9736 13.2054 24.0131 14.7669 23.9966C16.5263 23.9831 17.9438 22.0616 18.9503 20.5196C19.6718 19.4141 19.9403 18.8576 20.4998 17.6096C16.4303 16.0601 15.7778 10.2732 19.8053 8.05167Z" fill="white"/>
    </svg>;
    return (
        <div className = {classes.Smartphone}>
            <h1 className = {classes.Heading}>Available for your smartphone.</h1>
            <p className = {classes.Paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
            <button className = {classes.GooglePlayBtn}><image className = {classes.GPIcon}>{gpIcon}</image><span>Google Play</span></button>
            <button className = {classes.AppStoreBtn}><image className = {classes.APIcon}>{appleIcon}</image><span>App Store</span></button>
            <div className = {classes.Images}>
                <div className = {classes.imagesContainer}>
                    <img className = {classes.ImageBottom} src = "./images/phoneFront.png" />
                    <img className = {classes.ImageCenter} src = "./images/phoneFront.png" />
                    <img className = {classes.ImageTop}src = "./images/phoneFront.png" />
                </div>
            </div>
        </div>
    )
}
export default Smartphone;