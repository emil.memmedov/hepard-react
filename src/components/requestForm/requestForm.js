import classes from './requestForm.module.css';
import React,{Component} from 'react';

class RequestForm extends Component {
    state = {  
        options: 0,
        selectDown: false,
        selectValue: false
    }
    SelectClick = ()=>{
        this.setState({selectDown: !this.state.selectDown});
    }
    OptionClick = (index) =>{
        let value = false;
        if(index === 1){
            value = 'Moto';
        }
        else if(index === 2){
            value = 'Bike'
        }
        else if(index === 3){
            value = 'Car' 
        }
        this.setState({options: index,selectValue: value});
    }
    ExitPlace = () =>{
        this.setState({selectDown:false});
    }
    RadioChange = (e) =>{
        if(e.target.getAttribute('id') === 'bPartner'){
            document.getElementById('dPartner').checked = false;
        }
        else{
            document.getElementById('bPartner').checked = false;
        }
    }
    render() { 
        return ( 
        <div className = {classes.RequestForm}>
            <div className = {classes.Line}>
                <svg width="511" height="470" viewBox="0 0 511 470" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M-464.949 -281.819C-464.949 -281.819 -369.817 -358.409 -127.499 -388.278C24.9169 -407.065 146.034 -363.724 163.642 -273.496C179.942 -189.963 245.585 -152.731 389.877 -164.15C529.447 -128.174 623.454 97.7474 233.629 215.758C20.644 238.226 89.907 472.69 -135.908 469.124L-464.949 -281.819Z" fill="#D1DDF5" fill-opacity="0.07"/>
                </svg>
            </div>
            <div className = {classes.Image}>
                <img src = "./images/lastPhone.png" />
                <img src = "./images/requestRight.png" />
            </div>
            <div className = {classes.Form}>
                <h1 className = {classes.Heading}>Request Form</h1>
                <p className = {classes.P}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since </p>
                <form className = {classes.FormEl}>
                    <p className = {classes.Partners}>Partner Type</p>
                    <label for = "bPartner" className = {classes.BusinessPartner}>Business Partner</label>
                    <input id = "bPartner" className = {classes.bPartner} type ="radio" onClick = {this.RadioChange}/><span id = "bPartnerId"></span>
                    <label for = "dPartner" className = {classes.DeliveryPartner}>Delivery Partner</label>
                    <input id = "dPartner" className = {classes.dPartner} type ="radio" onClick = {this.RadioChange}/> <span id = "dPartnerID"></span>
                    <div className = {classes.Fillform}>
                        <label for = "pName" className = {classes.pName}>Person Name</label><input placeholder = "Your Name" type ="text" id = "pName" className = {classes.pNameInp}/>
                        <div className = {classes.ch}>
                            <label for = "vehicle" className = {classes.Vehicle}>Vehicle Type</label>
                            <div onClick = {this.SelectClick} className = {classes.selectClick}>
                                <input type = "text" className = {this.state.selectDown?[classes.VType,classes.Open].join(' ') :classes.VType} placeholder = "Your Vehicle Type" readOnly value = {this.state.selectValue !==false? this.state.selectValue:null}/>
                            </div>
                            <div className = {classes.ExitPlace} onClick = {this.ExitPlace} style = {{display: this.state.selectDown?'block':'none'}}>
                                <div className = {classes.selectDown} style = {{display: this.state.selectDown?'block': 'none'}}>
                                    <p id = "moto" onClick = {(index) =>this.OptionClick(1)} style = {{color: this.state.options === 1? '#2A52A0': '#565656'}} className = {this.state.options === 1?classes.Active:classes.None} >Moto</p>
                                    <p id = "bike" onClick = {(index) =>this.OptionClick(2)} style = {{color: this.state.options === 2? '#2A52A0': '#565656'}} className = {this.state.options === 2?classes.Active:classes.None}>Bike</p>
                                    <p id = "car" onClick = {(index) =>this.OptionClick(3)} style = {{color: this.state.options === 3? '#2A52A0': '#565656'}} className = {this.state.options === 3?classes.Active:classes.None}>Car</p>
                                </div>
                            </div>
                        </div>
                        <div className= {classes.ch}>
                            <label for = "city" className = {classes.City}>City</label><input placeholder = "Write your city" type ="text" id = "city" className = {classes.CityInp} />
                        </div>
                        <div className = {classes.ch}>
                            <label for = "phone" className = {classes.Phone}>Phone Number</label><input placeholder = "(994) 55 5555555" type ="text" id = "phone" className = {classes.PhoneInp} />
                        </div>
                        <button type = "submit" className = {classes.SendRequest}>Send Request Form</button>
                    </div>
                </form>
            </div>
        </div> );
    }
}
 
export default RequestForm;